/*
// Definition for a Node.
class Node {
    int val;
    Node next;
    Node random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}
*/

class Solution {
    public Node copyRandomList(Node head) {
        if (head == null)
            return null;
        
        // Step 1: Create new nodes and insert them next to original nodes
        Node curr = head;
        while (curr != null) {
            Node newNode = new Node(curr.val);
            newNode.next = curr.next;
            curr.next = newNode;
            curr = newNode.next;
        }
        
        // Step 2: Update random pointers of the new nodes
        curr = head;
        while (curr != null) {
            if (curr.random != null) {
                curr.next.random = curr.random.next;
            }
            curr = curr.next.next;
        }
        
        // Step 3: Separate original and copied lists
        Node original = head;
        Node copied = head.next;
        Node copiedHead = head.next;
        while (original != null) {
            original.next = original.next.next;
            copied.next = (copied.next != null) ? copied.next.next : null;
            original = original.next;
            copied = copied.next;
        }
        
        return copiedHead;
    }
}
